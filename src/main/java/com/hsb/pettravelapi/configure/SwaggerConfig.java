package com.hsb.pettravelapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "반려동물모임 App",
                description = "반려동물과 여행",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("반려동물모임 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
