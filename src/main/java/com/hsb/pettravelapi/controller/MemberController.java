package com.hsb.pettravelapi.controller;

import com.hsb.pettravelapi.model.member.MemberChangeRequest;
import com.hsb.pettravelapi.model.member.MemberResponse;
import com.hsb.pettravelapi.model.member.MemberCreateRequest;
import com.hsb.pettravelapi.model.member.MemberItem;
import com.hsb.pettravelapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/member-info/{id}")
    public String putMember(@PathVariable long id, @RequestBody MemberChangeRequest request){
        memberService.putMember(id, request);

        return "OK";
    }
}
