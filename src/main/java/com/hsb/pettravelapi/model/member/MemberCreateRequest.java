package com.hsb.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.cdi.Eager;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
}
