package com.hsb.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private String name;
    private String phoneNumber;
}
