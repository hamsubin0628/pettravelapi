package com.hsb.pettravelapi.model.pet;

import com.hsb.pettravelapi.emums.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetItem {
    private Long MemberId;
    private String MemberName;
    private String MemberPhoneNumber;
    private String petSize;
}
