package com.hsb.pettravelapi.model.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetResponse {
    private Long MemberId;
    private String MemberName;
    private String MemberPhoneNumber;
    private String petSize;
}
