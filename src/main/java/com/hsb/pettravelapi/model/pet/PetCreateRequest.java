package com.hsb.pettravelapi.model.pet;

import com.hsb.pettravelapi.emums.Size;
import com.hsb.pettravelapi.entity.Member;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private String petName;

    @Enumerated(value = EnumType.STRING)
    private Size size;
}
