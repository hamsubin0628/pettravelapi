package com.hsb.pettravelapi.repository;

import com.hsb.pettravelapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
