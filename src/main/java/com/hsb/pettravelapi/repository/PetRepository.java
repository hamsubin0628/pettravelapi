package com.hsb.pettravelapi.repository;

import com.hsb.pettravelapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
