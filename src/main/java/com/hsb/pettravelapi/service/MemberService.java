package com.hsb.pettravelapi.service;

import com.hsb.pettravelapi.entity.Member;
import com.hsb.pettravelapi.model.member.MemberChangeRequest;
import com.hsb.pettravelapi.model.member.MemberResponse;
import com.hsb.pettravelapi.model.member.MemberCreateRequest;
import com.hsb.pettravelapi.model.member.MemberItem;
import com.hsb.pettravelapi.model.pet.PetItem;
import com.hsb.pettravelapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for(Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }

    public void putMember(long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }
}
