package com.hsb.pettravelapi.service;

import com.hsb.pettravelapi.entity.Member;
import com.hsb.pettravelapi.entity.Pet;
import com.hsb.pettravelapi.model.pet.PetCreateRequest;
import com.hsb.pettravelapi.model.pet.PetItem;
import com.hsb.pettravelapi.model.pet.PetResponse;
import com.hsb.pettravelapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;


    public void setPet(Member member, PetCreateRequest request){
        Pet addData = new Pet();
        addData.setPetName(request.getPetName());
        addData.setMember(member);
        addData.setSize(request.getSize());

        petRepository.save(addData);
    }
    public List<PetItem> getPets(){
        List<Pet> originList = petRepository.findAll();

        List<PetItem> result = new LinkedList<>();

        for(Pet pet : originList){
            PetItem addItem = new PetItem();
            addItem.setMemberId(pet.getMember().getId());
            addItem.setMemberName(pet.getMember().getName());
            addItem.setMemberPhoneNumber(pet.getMember().getPhoneNumber());
            addItem.setPetSize(pet.getSize().getSizeEnum());

            result.add(addItem);
        }
        return result;
    }

    public PetResponse getPet(long id){
        Pet originData = petRepository.findById(id).orElseThrow();

        PetResponse response = new PetResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setPetSize(originData.getSize().getSizeEnum());

        return response;
    }
}
