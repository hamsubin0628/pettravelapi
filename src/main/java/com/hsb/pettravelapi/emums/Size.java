package com.hsb.pettravelapi.emums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Size {
    S("소형",10000.0),
    M("중형",15000.0),
    L("대형",20000.0);

    private final String sizeEnum;
    private final Double duesEnum;
}
